const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


//1. Get all items that are available

function availableItems(items) {
    const available = items.filter((item) => {
        return (item.available === true)
    })

    return available;
}

console.log("1-available items -", availableItems(items));

//2. Get all items containing only Vitamin C.

function onlyVitaminC(items) {
    const onlyC = items.filter((item) => {
        return item.contains === 'Vitamin C';
    })

    return onlyC;
}

console.log("2- only vitamin C items- ", onlyVitaminC(items))

//3. Get all items containing Vitamin A.

function vitaminAItems(items) {
    const vitaminA = items.filter((item)=>{
        return item.contains.includes('Vitamin A')
    })

    return vitaminA;
}

console.log("3- vitamin A items- ",vitaminAItems(items));

//4. Group items based on the Vitamins that they contain in the following format

// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

function groupingItems(items){
    const vitamins = []
    const groups = {}
    items.forEach((item)=>{
        item['vitamins'] = item.contains.split(", ")

        item['vitamins'].forEach((vitamin)=>{
            if(groups[vitamin]){
                groups[vitamin].push(item.name)
            }
            else{
                groups[vitamin] = [item.name]
            }
        })
    })
    return groups;
}

console.log(JSON.stringify(groupingItems(items),null,2));

//5. Sort items based on number of Vitamins they contain.

function sortedItems(items){
    const sorted = items.sort((item1,item2)=>{
        const value1 = item1.vitamins.length
        const value2 = item2.vitamins.length

        if( value1 < value2 ){
            return 1;
        }
        else if(value1 >value2){
            return -1;
        }
        return 0;
    })

    return sorted;
}

console.log(sortedItems(items));